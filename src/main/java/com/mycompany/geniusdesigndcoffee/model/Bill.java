/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.geniusdesigndcoffee.model;

import com.mycompany.geniusdesigndcoffee.dao.CustomerDao;
import com.mycompany.geniusdesigndcoffee.dao.EmployeeDao;
import com.mycompany.geniusdesigndcoffee.model.Customer;
import com.mycompany.geniusdesigndcoffee.model.Employee;
import com.mycompany.geniusdesigndcoffee.model.User;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class Bill {

    private int id;
    private String date;
    private double total;
    private double discount;
    private double cash;
    private double change;
    private Employee empID;
    private Customer cusID;

    public Bill(int id, String date, double total, double discount, double cash, double change, Employee empID, Customer cusID) {
        this.id = id;
        this.date = date;
        this.total = total;
        this.discount = discount;
        this.cash = cash;
        this.change = change;
        this.empID = empID;
        this.cusID = cusID;
    }

    public Bill() {
        this.id = -1;
    }
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public Employee getEmpID() {
        return empID;
    }

    public void setEmpID(Employee empID) {
        this.empID = empID;
    }

    public Customer getCusID() {
        return cusID;
    }

    public void setCusID(Customer cusID) {
        this.cusID = cusID;
    }

    @Override
    public String toString() {
        return "Bill{" + "id=" + id + ", date=" + date + ", total=" + total + ", discount=" + discount + ", cash=" + cash + ", change=" + change + ", empID=" + empID 
                + ", cusID=" + cusID 
                + '}';
    }
    
    public static Bill fromRS(ResultSet rs) {
        Bill bill = new Bill();
        EmployeeDao emp = new EmployeeDao();
        CustomerDao cus = new CustomerDao();
        try {
            bill.setId(rs.getInt("BILL_ID"));
            bill.setDate(rs.getString("BILL_DATE"));
            bill.setTotal(rs.getDouble("BILL_TOTAL"));
            bill.setDiscount(rs.getDouble("BILL_DISCOUNT"));
            bill.setCash(rs.getDouble("BILL_CASH"));
            bill.setChange(rs.getDouble("BILL_CHANGE"));
            int empID = rs.getInt("EMP_ID");
            Employee item = emp.get(empID);
            bill.setEmpID(item);
            int cusID = rs.getInt("EMP_ID");
            Customer item2 = cus.get(cusID);
            bill.setCusID(item2);
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return bill;
    }
}
