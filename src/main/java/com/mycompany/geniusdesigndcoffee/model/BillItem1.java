/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.geniusdesigndcoffee.model;

import com.mycompany.geniusdesigndcoffee.Ui.BillItem;
import com.mycompany.geniusdesigndcoffee.dao.BillDao;
import com.mycompany.geniusdesigndcoffee.dao.ProductDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author ASUS
 */
public class BillItem1 {

    
    
    private int id;
    private int amount;
    private String name;
    private double price;
    private Product prodID;
    private Bill billID;
    //double totalPrice;

    public BillItem1(int id, int amount, String name, double price, Product prodID, Bill billID) {
        this.id = id;
        this.amount = amount;
        this.name = name;
        this.price = price;
        this.prodID = prodID;
        this.billID = billID;
    }
    
    public BillItem1() {
        this.id = -1;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }
    
//    public double getTotalPrice() {
//        return prodID.getPrice()*amount;
//    }
//    
//    public void setTotalPrice(Product product) {
//        this.totalPrice = product.getPrice()*amount;
//    }
    
    public void setPrice(double price) {
        this.price = price;
    }

    public Product getProdID() {
        return prodID;
    }

    public void setProdID(Product prodID) {
        this.prodID = prodID;
    }

    public Bill getBillID() {
        return billID;
    }

    public void setBillID(Bill billID) {
        this.billID = billID;
    }

    @Override
    public String toString() {
        return "BillItem{" + "id=" + id + ", amount=" + amount + ", name=" + name + ", price=" + price + ", prodID=" + prodID + ", billID=" + billID + '}';
    }
    
    public static BillItem1 fromRS(ResultSet rs) {
        BillItem1 billItem = new BillItem1();
        ProductDao prod = new ProductDao();
        BillDao bill = new BillDao();
        try {
            billItem.setId(rs.getInt("BILL_ITEM_ID"));
            billItem.setAmount(rs.getInt("AMOUNT"));
            billItem.setName(rs.getString("BILL_ITEM_NAME"));
            billItem.setPrice(rs.getDouble("BILL_ITEM_PRICE"));
            
//            int prodID = rs.getInt("PRODUCT_ID");
//            Product item = prod.get(prodID);
//            billItem.setProdID(item);
//            int billID = rs.getInt("BILL_ID");
//            Bill item2 = bill.get(billID);
//            billItem.setBillID(item2);
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return billItem;
    }
}
