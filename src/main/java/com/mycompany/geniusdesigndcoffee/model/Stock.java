/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.geniusdesigndcoffee.model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class Stock {

    private int id;
    private String name;
    private int quantity;
    private int min;
    private int max;
    private String stockProd;
    private String stockExp;

    public Stock(int id, String name, int quantity, int min, int max, String stockProd, String stockExp) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.min = min;
        this.max = max;
        this.stockProd = stockProd;
        this.stockExp = stockExp;
    }

    public Stock() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public String getStockProd() {
        return stockProd;
    }

    public void setStockProd(String stockProd) {
        this.stockProd = stockProd;
    }

    public String getStockExp() {
        return stockExp;
    }

    public void setStockExp(String stockExp) {
        this.stockExp = stockExp;
    }

    @Override
    public String toString() {
        return "Stock{" + "id=" + id + ", name=" + name + ", quantity=" + quantity + ", min=" + min + ", max=" + max + ", stockProd=" + stockProd + ", stockExp=" + stockExp + '}';
    }
    
    public static Stock fromRS(ResultSet rs) {
        Stock stock = new Stock();
        try {
            stock.setId(rs.getInt("STOCK_ID"));
            stock.setName(rs.getString("STOCK_NAME"));
            stock.setQuantity(rs.getInt("STOCK_QUANTITY"));
            stock.setMin(rs.getInt("STOCK_MIN"));
            stock.setMax(rs.getInt("STOCK_MAX"));
            stock.setStockProd(rs.getString("STOCK_PROD"));
            stock.setStockExp(rs.getString("STOCK_EXP"));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return stock;
    }
}
