/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.geniusdesigndcoffee.model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class Employee {
    private int id;
    private String name;
    private String lastname;
    private String tel;
    private String gender;
    private String date;
    private String status;

    public Employee(int id, String name, String lastname, String tel, String gender, String date, String status) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.tel = tel;
        this.gender = gender;
        this.date = date;
        this.status = status;
    }

    public Employee() {
        this.id = -1;
        this.name = "";
        this.lastname ="";
        this.tel = "";
        this.gender = "";
        this.date = "";
        this.status = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", lastname=" + lastname + ", tel=" + tel + ", gender=" + gender 
                + ", date=" + date + ", status=" + status + '}';
    }
    
    public static Employee fromRS(ResultSet rs){
        Employee emp = new Employee();
        try {
            emp.setId(rs.getInt("EMP_ID"));
            emp.setName(rs.getString("EMP_NAME"));
            emp.setLastname(rs.getString("EMP_LASTNAME"));
            emp.setTel(rs.getString("EMP_TEL"));
            emp.setGender(rs.getString("EMP_GENDER"));
            emp.setStatus(rs.getString("EMP_STATUS"));
            emp.setDate(rs.getString("EMP_START_DATE"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return emp;
    }
    
    
    
}
