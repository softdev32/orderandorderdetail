/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.geniusdesigndcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class Customer {
    private int id;
    private String name;
    private String lastname;
    private String tel;
    private int point;

    public Customer(int id,String name, String lastname, String tel, int point) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.tel = tel;
        this.point = point;
    }

    public Customer(String name, String lastname, String tel, int point) {
        this.id = -1;
        this.name = name;
        this.lastname = lastname;
        this.tel = tel;
        this.point = point;
    }

    public Customer() {
        this.id = -1;
        this.tel= "09099090";
        this.point=0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastname;
    }

    public void setLastName(String lastname) {
        this.lastname = lastname;
    }
    
    public void setTel(String tel) {
        this.tel = tel;
    }
    public String getTel() {
        return tel;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }


    @Override
    public String toString() {
        return "Customer{" + " name=" + name + ", lastname=" + lastname + ", tel=" + tel +  "point=" + point + '}';
    }

    
    public static Customer fromRS(ResultSet rs) {
        Customer Customer = new Customer();
        try {
            Customer.setId(rs.getInt("CUS_ID"));
            Customer.setName(rs.getString("CUS_NAME"));
            Customer.setLastName(rs.getString("CUS_LASTNAME"));
            Customer.setTel(rs.getString("CUS_TEL"));
            Customer.setPoint(rs.getInt("CUS_POINT"));
        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return Customer;
    }
}
