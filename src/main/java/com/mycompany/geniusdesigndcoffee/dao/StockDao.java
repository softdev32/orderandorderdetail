/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.geniusdesigndcoffee.dao;

import com.mycompany.geniusdesigndcoffee.helper.DatabaseHelper;
import com.mycompany.geniusdesigndcoffee.model.Stock;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class StockDao implements Dao<Stock>{

    @Override
    public Stock get(int id) {
        Stock item = null;
        String sql = "SELECT * FROM STOCK WHERE STOCK_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Stock.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<Stock> getAll() {
        ArrayList<Stock> list = new ArrayList();
        String sql = "SELECT * FROM STOCK";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                Stock stock = Stock.fromRS(rs);
                list.add(stock);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    @Override
    public List<Stock> getAll(String where, String order) {
        ArrayList<Stock> list = new ArrayList();
        String sql = "SELECT * FROM STOCK ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Stock stock = Stock.fromRS(rs);
                list.add(stock);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Stock save(Stock obj) {
    String sql = "INSERT INTO STOCK (STOCK_NAME, STOCK_QUANTITY, STOCK_MIN, STOCK_MAX, STOCK_PROD, STOCK_EXP)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getQuantity());
            stmt.setInt(3, obj.getMin());
            stmt.setInt(4, obj.getMax());
            stmt.setString(5, sdf.format(obj.getStockProd()));
            stmt.setString(6, sdf.format(obj.getStockExp()));
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;     
    }

    @Override
    public Stock update(Stock obj) {
        String sql = "UPDATE STOCK"
                + " SET STOCK_NAME = ?, STOCK_QUANTITY = ?, STOCK_MIN = ?, STOCK_MAX = ?, STOCK_PROD = ?, STOCK_EXP = ?"
                + " WHERE STOCK_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getQuantity());
            stmt.setInt(3, obj.getMin());
            stmt.setInt(4, obj.getMax());
            stmt.setString(5, sdf.format(obj.getStockProd()));
            stmt.setString(6, sdf.format(obj.getStockExp()));
            
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Stock obj) {
        String sql = "DELETE FROM STOCK WHERE STOCK_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
    
    public List<Stock>getByName(String name) {
        ArrayList<Stock> list = new ArrayList();
        String sql = "SELECT * FROM STOCK WHERE STOCK_NAME LIKE ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "%" + name + "%");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Stock stock = Stock.fromRS(rs);
                list.add(stock);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    
    
}
