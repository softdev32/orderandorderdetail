/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.geniusdesigndcoffee.dao;

import com.mycompany.geniusdesigndcoffee.Ui.BillItem;
import com.mycompany.geniusdesigndcoffee.dao.Dao;
import com.mycompany.geniusdesigndcoffee.helper.DatabaseHelper;
import com.mycompany.geniusdesigndcoffee.model.BillItem1;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class BillItemDao implements Dao<BillItem1>{

    @Override
    public BillItem1 get(int id) {
        BillItem1 item = null;
        String sql = "SELECT * FROM BILL_ITEM WHERE BILL_ITEM_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = BillItem1.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<BillItem1> getAll() {
        ArrayList<BillItem1> list = new ArrayList();
        String sql = "SELECT * FROM BILL_ITEM";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                BillItem1 billItem = BillItem1.fromRS(rs);
                list.add(billItem);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<BillItem1> getAll(String where, String order) {
        ArrayList<BillItem1> list = new ArrayList();
        String sql = "SELECT * FROM BILL_ITEM where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                BillItem1 billitem = BillItem1.fromRS(rs);
                list.add(billitem);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    public List<BillItem1>getByName(String name) {
        ArrayList<BillItem1> list = new ArrayList();
        String sql = "SELECT * FROM BILL_ITEM WHERE BILL_ITEM_NAME LIKE ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "%" + name + "%");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                BillItem1 billItem = BillItem1.fromRS(rs);
                list.add(billItem);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<BillItem1>getByBillItemID(int id) {
        ArrayList<BillItem1> list = new ArrayList();
        String sql = "SELECT * FROM BILL_ITEM WHERE BILL_ID LIKE ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "%" + id + "%");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                BillItem1 billItem = BillItem1.fromRS(rs);
                list.add(billItem);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public BillItem1 save(BillItem1 obj) {
        String sql = "INSERT INTO BILL (BILL_ITEM_ID, AMOUNT, BILL_ITEM_NAME, BILL_ITEM_PRICE, PRODUCT_ID, BILL_ID)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            stmt.setInt(1, obj.getId());
            stmt.setInt(2, obj.getAmount());
            stmt.setString(3, obj.getName());
            stmt.setDouble(4, obj.getPrice());
            stmt.setInt(5, obj.getProdID().getId());
            stmt.setInt(6, obj.getBillID().getId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj; 
    }

    @Override
    public BillItem1 update(BillItem1 obj) {
        String sql = "UPDATE BILL_ITEM"
                + " SET AMOUNT = ?, BILL_ITEM_NAME = ?, BILL_ITEM_PRICE = ?, PRODUCT_ID = ?, BILL_ID = ?"
                + " WHERE BILL_ITEM_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getAmount());
            stmt.setString(2, obj.getName());
            stmt.setDouble(3, obj.getPrice());
            stmt.setInt(4, obj.getProdID().getId());
            stmt.setInt(5, obj.getBillID().getId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(BillItem1 obj) {
        String sql = "DELETE FROM BILL_ITEM WHERE BILL_ITEM_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }



}
