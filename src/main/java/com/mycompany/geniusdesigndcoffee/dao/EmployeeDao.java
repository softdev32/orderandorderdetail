/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.geniusdesigndcoffee.dao;

import com.mycompany.geniusdesigndcoffee.helper.DatabaseHelper;
import com.mycompany.geniusdesigndcoffee.model.Employee;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class EmployeeDao implements Dao<Employee>{

    @Override
    public Employee get(int id) {
        Employee emp = null;
        String sql = "SELECT * FROM EMPLOYEE WHERE EMP_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next()){
               emp = Employee.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return emp;    }

    @Override
    public List<Employee> getAll() {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM EMPLOYEE";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                Employee emp = Employee.fromRS(rs);
                list.add(emp);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;    
    }
    
    @Override
    public List<Employee> getAll(String where, String order) {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM EMPLOYEE where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Employee emp = Employee.fromRS(rs);
                list.add(emp);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Employee> getAll(String order) {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM EMPLOYEE ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Employee emp = Employee.fromRS(rs);
                list.add(emp);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Employee>getByName(String name) {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM EMPLOYEE WHERE EMP_NAME LIKE ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "%" + name + "%");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Employee employ = Employee.fromRS(rs);
                list.add(employ);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }


    @Override
    public Employee save(Employee obj) {
        String sql = "INSERT INTO EMPLOYEE (EMP_NAME, EMP_LASTNAME, EMP_TEL, EMP_GENDER, EMP_START_DATE,EMP_STATUS)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getLastname());
            stmt.setString(3, obj.getTel());
            stmt.setString(4, obj.getGender());
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//            stmt.setString(5, sdf.format(obj.getDate()));
            stmt.setString(5, obj.getDate());
            stmt.setString(6, obj.getStatus());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;    
    }

    @Override
    public Employee update(Employee obj) {
        String sql = "UPDATE EMPLOYEE"
                + " SET EMP_NAME = ?, EMP_LASTNAME = ?, EMP_TEL = ?, EMP_GENDER = ?, EMP_START_DATE = ?, EMP_STATUS = ?"
                + " WHERE EMP_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getLastname());
            stmt.setString(3, obj.getTel());
            stmt.setString(4, obj.getGender());
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//            stmt.setString(5, sdf.format(obj.getDate()));
            stmt.setString(5, obj.getDate());
            stmt.setString(6, obj.getStatus());
            stmt.setInt(7, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }    
    }

    @Override
    public int delete(Employee obj) {
        String sql = "DELETE FROM EMPLOYEE WHERE EMP_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;    
    } 
}
