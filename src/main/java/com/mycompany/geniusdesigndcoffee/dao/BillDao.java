/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.geniusdesigndcoffee.dao;

import com.mycompany.geniusdesigndcoffee.helper.DatabaseHelper;
import com.mycompany.geniusdesigndcoffee.model.Bill;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class BillDao implements Dao<Bill>{

    @Override
    public Bill get(int id) {
        Bill item = null;
        String sql = "SELECT * FROM BILL WHERE BILL_ID=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Bill.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<Bill> getAll() {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM BILL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                Bill bill = Bill.fromRS(rs);
                list.add(bill);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Bill> getAll(String where, String order) {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM BILL ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Bill bill = Bill.fromRS(rs);
                list.add(bill);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public Bill save(Bill obj) {
        String sql = "INSERT INTO BILL (BILL_DATE, BILL_TOTAL, BILL_DISCOUNT, BILL_CASH, BILL_CHANGE, EMP_ID, CUS_ID)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            stmt.setString(1, obj.getDate());
            stmt.setDouble(2, obj.getTotal());
            stmt.setDouble(3, obj.getDiscount());
            stmt.setDouble(4, obj.getCash());
            stmt.setDouble(5, obj.getChange());
            stmt.setInt(6, obj.getEmpID().getId());
            stmt.setInt(7, obj.getCusID().getId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj; 
    }

    @Override
    public Bill update(Bill obj) {
        String sql = "UPDATE BILL"
                + " SET BILL_DATE = ?, BILL_TOTAL = ?, BILL_DISCOUNT = ?, BILL_CASH = ?, BILL_CHANGE = ?, EMP_ID = ?, CUS_ID = ?"
                + " WHERE BILL_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            stmt.setString(1, obj.getDate());
            stmt.setDouble(2, obj.getTotal());
            stmt.setDouble(3, obj.getDiscount());
            stmt.setDouble(4, obj.getCash());
            stmt.setDouble(5, obj.getChange());
            stmt.setInt(6, obj.getEmpID().getId());
            stmt.setInt(7, obj.getCusID().getId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Bill obj) {
        String sql = "DELETE FROM BILL WHERE BILL_ID = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<Bill> getByDate(String date) {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM BILL WHERE BILL_DATE LIKE ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, date + "%");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Bill bill = Bill.fromRS(rs);
                list.add(bill);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
}