/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.geniusdesigndcoffee.Ui;

import com.mycompany.geniusdesigndcoffee.model.Product;
import java.sql.ResultSet;

/**
 *
 * @author Admin
 */
public class BillItem {
    
    Product product;
    int amount;
    double totalPrice;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
    
    public double getTotalPrice() {
        return product.getPrice()*amount;
    }
    
    public void setTotalPrice(Product product) {
        this.totalPrice = product.getPrice()*amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public BillItem(Product product, int amount) {
        this.product = product;
        this.amount = amount;
    }
}
